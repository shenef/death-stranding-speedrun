# Death Stranding Speedrun Routes

## Route Documents:
- [Any% Offline Very Easy](https://gitlab.com/shenef/death-stranding-speedrun/blob/master/any%25_offline_very_easy.md)

## How to contribute
- create an account on gitlab.com
- click the route document you want to contribute to
- click on "Edit" on the top right of the document
- take a look at the [markdown guide](https://www.markdownguide.org/basic-syntax/)
- edit the document (check out the "Preview" button at the top)
- once you are done enter a commit message (short summary of what you changed)
- click "Commit changes"

Contact shenef#1269 on discord if you want to create a document for another category or difficulty.